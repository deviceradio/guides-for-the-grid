# Getting Started with The Grid

This guide will walk you through some of the basics of managing your connected devices on The Grid

[Adding your first Device](/getting-started/getting-started.md#adding-your-first-device)

[Moving a Device](/getting-started/getting-started.md#moving-a-device)

[Applications & Adapters](/getting-started/getting-started.md#applications-adapters)

[Workspaces](/getting-started/getting-started.md#workspaces)
