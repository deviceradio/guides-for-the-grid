# Getting Started

This guide will walk you through some of the basics of managing your connected devices on The Grid.

## Adding your first device

- Navigate to **Inventories**.

- This is your personal inventory, you can read more about it in the Details tab. When you are ready to continue, click on **Add Device** then **Create new**.

- Clicking the question mark next to the title field will generate a random title for your device.

 All the fields are optional and the Product ID is only needed if your device is based on a Product, but we won’t get into that right now.

- Click **Create** when you are done.

- Existing devices can be added using their ID. Copy the **Device ID** for the next step.

 When you are ready to continue, head back to inventories.

## Moving a Device

- Create a new inventory to move the device into.

- Inventories only have a title and a description both are optional and clicking create without filling them will generate a random name.

- This is your first inventory in this workspace. Let’s add that device to it using the ID you copied. Click on **Add Device** then **Add by ID** and paste the ID there.

- You can now return to the **Personal Inventory** and remove the Device from there. Click the **ellipses** on the corner of the device card and click **Remove**.

 Make sure you leave the checkbox for deleting empty, otherwise the device will be permanently deleted from The Grid and not just this inventory.

- Continue to the next step when you are ready.

## Applications & Adapters

External apps, services and data sources can interact with The Grid as **Applications**.

For example We’ve created an adapter for the ***Dark Sky weather service*** that you can use to create weather devices for a location of your choosing.

- Head over to [Dark Sky weather for The Grid](http://darksky.demo.deviceradio.net/) to create such a device.

- You will have to grant some permissions when they are requested to allow the adapter to create the device.

- Drag and drop the pin to the location you want on the map. Select an inventory for the device from the sidebar and fill in the name at the top of the page.

- When you are done click **Create the device**.

- External services like The Dark Sky weather adapter are considered applications on The Grid. You can now see it in the **Approved Applications** view.

- If you wish to develop an external application head over to **Applications** to see how they work. **Create** a new application to use as an example.

- **Service Accounts** can be assigned to an application to allow it to log in independently. For this example you can leave it blank.

- The Grid uses ***OAuth2*** to authenticate external applications and services. You can generate a secret key here to use in your application.

- You can also Edit the OAuth2 settings here to define the scopes the application will request from the user.

## Workspaces

- Workspaces can be used to further organize your devices and projects and work with others.

- Your Personal Workspace is currently the active workspace. Clicking **Manage Workspaces** will take you to the workspace management view.

 Here you can create new workspaces, manage them and switch between them.

- Create a new workspace now.

- Selecting a workspace from the list sets it as the **Active Workspace**.

- You’ll notice that the new workspace has no inventories or applications. Head over to Inventories again.

- Notice that the Personal Inventory is still visible. This inventory will always be visible across all workspaces.

 Any new inventories, devices or applications you create will belong to the current active workspace.

- That’s it for this guide! We can’t wait to see what you create using The Grid!
